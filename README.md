# svc-master

## Requirements

- Node.JS v16 LTS or above
- Relational Databse Server (MSSQL, MySQL, `Postgres`)
- MongoDB
- Redis
- RabbitMQ

## Deployment

### To get the Node server running locally

- Clone this repo
- `cd /path/to/cloned/repo`
- `npm install` to install all required dependencies
- Run `cp ..env.example .env` and then edit a file with your fav editor, example `vi .env`
- You need to setup certificate file for JWT purpose, by copying public to `./certificate`

  Or generate certificate files by `bash ./bin/asymmetric-keypair.sh 2048`.

  This command will generate 3 kind of certificate file:
    - `./certificate/private.key` which is required for auth server
    - `./certificate/private.pkcs8.key` which is required for auth server
    - `./certificate/public.key` this file you can put  anywhere

- Run `npm run migrate` to batch run of database migration script
- Run `npm start` to start the local server
- The API is available at `http://localhost:3000`

### Docker

#### Build image
```sh
docker build --rm -t svc-master -f Dockerfile .
```

#### Run image (test mode)
```sh
docker run -it --rm -p 3000:3000 -p 30000:30000 svc-master
```

#### Run image
```sh
docker run -d -t -p 3000:3000 -p 30000:30000 svc-master
```

#### With passing env file
```sh
docker run -d -t -p 3000:3000 -p 30000:30000 --env-file ./.env svc-master
```

## Design pattern
Descriptions:
1. `Model`: a DTO table/collection database mapping
2. `Repository`: Database accessor could be read (include join/union), write, or maybe delete
3. `Service`: Business logic
4. `Handler`: A `Service` implements, usually in controller e.g. grpc, api, io etc.

See this diagram
<br>
<br>
<img src=".doc/design-pattern.jpg" alt="MarineGEO circle logo" style="width:100%;"/>

## Commands

### Application
Install dependency
```sh
npm install
```

Start service
```sh
npm start
```

Start service if you are a developer
```sh
npm run start:dev
```

Build (web pages and static files)
```sh
npm run build
```

Run unit test
```sh
npm test
```

Update dependency
```sh
npm run module-update
```

Generate SQL model
```sh
npm run sql:model
```

### RDBMS
Create database
```sh
npm run sql:create
```

Drop database
```sh
npm run sql:drop
```

Up migration (all)
```sh
npm run sql:migrate
```

Down/undo migration (one)
```sh
npm run sql:migrate:undo
```

Down/undo migration (all)
```sh
npm run sql:migrate:undo:all
```

### Code convention
Lint your code
```sh
npm run lint
```

Lint and fix your code
```sh
npm run lint:fix
```

### Unit Test
```sh
npm test
```

### Certificate
Generate asymmetric certificate `(recommended)`
```sh
./bin/asymmetric-keypair.sh 4096
```
> Keep it in your mind! Generating JWT is exclusive by auth service. So, only auth service can accessing the private key.
> 
> And put the shared/public key in other service, like resource service. In case for verify JWT validation

### JSON Web Token (JWT)
Generate token
```sh
node ./src/util/generate-token.js generate '{"sub":"andaikan@nodomain.com","profile":{"accountId":"1","usrRegistrantId":"1","uname":"andaikan","email":"andaikan@nodomain.com","phone":"+6286313149xxx","name":"Andaikan"},"roles":["owner"],"scope":"offline_access","client_id":"all-in-one-client","aud":"http://localhost:3001/xyz"}'
```

Decode token
```sh
node ./src/util/generate-token.js decode 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhbmRhaWthbkBub2RvbWFpbi5jb20iLCJwcm9maWxlIjp7ImFjY291bnRJZCI6IjEiLCJ1c3JSZWdpc3RyYW50SWQiOiIxIiwidW5hbWUiOiJhbmRhaWthbiIsImVtYWlsIjoiYW5kYWlrYW5Abm9kb21haW4uY29tIiwicGhvbmUiOiIrNjI4NjMxMzE0OXh4eCIsIm5hbWUiOiJBbmRhaWthbiJ9LCJyb2xlcyI6WyJvd25lciJdLCJzY29wZSI6Im9mZmxpbmVfYWNjZXNzIiwiY2xpZW50X2lkIjoiYWxsLWluLW9uZS1jbGllbnQiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMwMDEveHl6IiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDozMDAxIiwiZXhwIjoxNzAzNjUzNjU5LCJpYXQiOjE2NzIxMTM2NTl9.SJzKYGh-bzWqAcsdIWJ0jFoNWQne-vlrTxLYa4RABpyfe4ML4AF7usdk0_hvIIPJmzCPOwTrRH8MLsn0coCIhBB0Cmrd52LM-8fSLOA6Y9JyCyX5b8NNVgg7ocCz2KclR1ug-4cSJr48Lpw_kvy2CoEQfwtdCDa7xSRqDVyKMQCvzBbWHT-36LsbqDRd8ZNzp2biHNp6qVfRN74ElFT_mmMmHWVh5uYgkM8Y4rDtK7mWya3iB89ga7YTMFVwx7gxG17AO4oqfIXF3m_cw38-xiORMv_82GiHbjxvwIBmVqXptPqb_0s8Y7jSGX6OLdcaMYjuzIIhwsNtss_w_gJarA'
```

### NPM Script Dependencies
```text
npm run <cmd_name>                # see package.json and scripts key
├── prepare
├── install
│   └── prepare
│
├── build
├── app
├── app:dev
├── deploy
│   ├── migrate
│   │   ├── sql:create
│   │   ├── sql:migrate
│   │   └── sql:model
│   ├── test
│   │   └── pretest
│   │   │   ├── pretest:service-a
│   │   │   ├── sql:model
│   │   │   └── build
│   │   ├── pretest:service-b
│   │   │   ├── sql:model
│   │   │   └── build
│   │   ├──  ...
│   │   ├── sql:model
│   │   └── build
│   └── start
│
├── lint
├── lint:fix
├── sql:create
├── sql:drop
├── sql:migrate
├── sql:migrate:undo
├── sql:migrate:undo:all
├── sql:model
│
├── migrate
│   ├── sql:create
│   ├── sql:migrate
│   └── sql:model
│
├── migrate:undo
│   ├── sql:create
│   ├── sql:migrate:undo
│   └── sql:model
│
├── start
│   ├── build
│   └── app
|
├── start:dev
│   ├── build
│   └── app:dev
|
├── pretest
│   ├── pretest:service-a
│   │   ├── sql:model
│   │   └── build
│   ├── pretest:service-b
│   │   ├── sql:model
│   │   └── build
│   ├── ...
│   ├── sql:model
│   └── build
│
├── pretest:service-a             # this will run `env-cmd npm explore service-a`
│   ├── sql:model
│   └── build
|
├── test
│   └── pretest
│       ├── pretest:service-a
│       │   ├── sql:model
│       │   └── build
│       ├── pretest:service-b
│       │   ├── sql:model
│       │   └── build
│       ├── ...
│       ├── sql:model
│       └── build
|
└── module-update
    ├── update
    └── install
        └── prepare
```

## Version Control Systems 

### Git Commit
Within `husky`, for every git commit will trigger unit testing for make sure changes working well.

### Versioning
We're using [`semver`](https://semver.org) for project versioning rule