// In this file you can configure migrate-mongo

require('dotenv').config();

const path = require('path');
const { env } = process;

const username = env.MASTER_MONGO_USERNAME || env.MONGO_USERNAME;
const password = env.MASTER_MONGO_PASSWORD || env.MONGO_PASSWORD;
const host = env.MASTER_MONGO_HOST || env.MONGO_HOST;
const port = env.MASTER_MONGO_PORT || env.MONGO_PORT;
const dbname = env.MASTER_MONGO_DBNAME || env.MONGO_DBNAME;

const config = {
  mongodb: {
    url: `mongodb://${username}:${password}@${host}:${port}/`,
    databaseName: dbname,
    options: {
      useNewUrlParser: true, // removes a deprecation warning when connecting
      useUnifiedTopology: true, // removes a deprecating warning when connecting
      //   connectTimeoutMS: 3600000, // increase connection timeout to 1 hour
      //   socketTimeoutMS: 3600000, // increase socket timeout to 1 hour
    }
  },

  // The migrations dir, can be an relative or absolute path. Only edit this when really necessary.
  migrationsDir: path.resolve('migration', 'mongo'),

  // The mongodb collection where the applied changes are stored. Only edit this when really necessary.
  changelogCollectionName: 'changelog',

  // The file extension to create migrations and search for in migration dir
  migrationFileExtension: '.js',

  // Enable the algorithm to create a checksum of the file contents and use that in the comparison to determine
  // if the file should be run.  Requires that scripts are coded to be run multiple times.
  useFileHash: false,

  // Don't change this, unless you know what you're doing
  moduleSystem: 'commonjs',
};

module.exports = config;
