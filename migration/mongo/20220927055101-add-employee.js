const Promise = require('bluebird');

const getEmployees = async () => {
  const employee = [
    {
      name: 'Afa',
      email: 'ifundeasy@gmail.com'
    }
  ];

  return employee;
}

const getExisting = async (db) => {
  const employees = await getEmployees()
  const selection = await db.collection('employee').find({
    email: {
      $in: employees.map(el => el.email)
    }
  });

  return selection
}

module.exports = {
  getEmployees,
  getExisting,
  async up(db, client) {
    const existData = await getExisting(db)

    let employees = []
    if (existData.length) {
      employees = existData;
    } else {
      employees = await getEmployees()
    }

    return existData.length ? true : Promise.all(employees.map((payload) => db.collection('employee').updateOne(
      { email: payload.email },
      { $setOnInsert: { ...payload } },
      { upsert: true }
    )))
  },

  async down(db, client) {
    const employees = await getEmployees()

    return db.collection('employee').deleteMany({
      email: {
        $in: employees.map((el) => el.email)
      }
    })
  }
};
