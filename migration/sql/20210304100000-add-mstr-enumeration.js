/* eslint-disable no-param-reassign, no-restricted-syntax */

const Enum = {
  app: {
    beMaster: 'svc-master',
    beOAuth: 'svc-oauth',
    beUser: 'svc-user',
    beCourier: 'svc-courier',
    beDocument: 'svc-document',
    feAdmin: 'fe-web-admin',
    feApp: 'fe-web-app',
    feMobile: 'fe-mobile',
  },
  httpMethod: {
    add: 'POST',
    get: 'GET',
    remove: 'DELETE',
    update: 'PUT'
  },
  isActive: {
    active: '1',
    deactive: '0',
    deleted: '-1'
  },
  verifyStatus: {
    pending: 'pending',
    accepted: 'accepted',
    refused: 'refused'
  },
  courierType: {
    sms: 'sms',
    email: 'email',
    whatsapp: 'whatsapp',
    telephone: 'telephone',
    notification: 'notification'
  },
  timeUnitType: {
    second: 'second',
    minute: 'minute',
    hour: 'hour',
    day: 'day',
    week: 'week',
    month: 'month',
    year: 'year'
  },
  measureUnitType: {
    k: 'kilo',
    h: 'hekto',
    m: 'meter'
  },
  documentType: {
    audio: 'audio',
    picture: 'picture',
    video: 'video',
    file: 'file'
  },
  documentScope: {
    identity: 'identity',
    portfolio: 'portfolio',
    attainment: 'attainment',
    expertise: 'expertise',
    experience: 'experience',
    article: 'article'
  },
  paymentType: {
    cash: 'cash',
    gift: 'gift',
    credit: 'credit card',
    debit: 'debit card',
    prepaid: 'prepaid card',
    transfer: 'bank transfer',
    va: 'virtual account',
    mobile: 'mobile payment',
    crypto: 'crypto currency',
    aggregator: 'aggregator',
    onlieCredit: 'online credit card',
    other: 'other'
  }
};

function getEnumeration() {
  return Enum
}

function flattenNestedObj(obj, temp = {}, key = '') {
  for (const k in obj) {
    if (typeof obj[k] === 'object') {
      flattenNestedObj(obj[k], temp, key ? `${key}.${k}` : k);
    } else {
      const kk = key ? `${key}.${k}` : k
      temp[kk] = obj[k];
    }
  }
  return Object.keys(temp).map((k) => ({
    key: k,
    value: temp[k],
    createdBy: 'SYSTEM',
    notes: 'autocreated'
  }))
}

module.exports = {
  getEnumeration,
  flattenNestedObj,
  up: async (queryInterface, Sequelize) => queryInterface.bulkInsert('mstrEnumerations', flattenNestedObj(Enum)),
  down: async (queryInterface, Sequelize) => {
    const { Op } = Sequelize;
    const condition = {
      [Op.or]: flattenNestedObj(Enum).map((record) => ({
        ...record,
        updatedBy: null,
        updatedAt: null
      }))
    }
    return queryInterface.bulkDelete('mstrEnumerations', condition)
  }
}
