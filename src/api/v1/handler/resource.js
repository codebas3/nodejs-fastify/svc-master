const qsToSQL = require('../../../util/qs-to-sql');
const { v1: { Resource: ResourceService } } = require('../../../service');

const create = async (req, res, next) => {
  try {
    const resourceService = new ResourceService()
    const result = await resourceService.create(req.body, null, req.logged.accountId);

    return res.send({
      code: 200,
      status: 'OK',
      data: [
        { ...result.dataValues }
      ]
    });
  } catch (e) {
    return next(e)
  }
};

const getOneBy = async (req, res, next) => {
  try {
    const resourceService = new ResourceService()
    const options = {
      where: { id: req.params.id },
      count: true
    }
    const result = await resourceService.getBy(options);

    return res.send({
      code: 200,
      status: 'OK',
      data: result
    });
  } catch (e) {
    return next(e)
  }
};

const getBy = async (req, res, next) => {
  try {
    const resourceService = new ResourceService()
    const options = {
      ...qsToSQL.build(req.query),
      count: true,
      distinct: true
    }
    const result = await resourceService.getBy(options);
    return res.send({
      code: 200,
      status: 'OK',
      data: result
    });
  } catch (e) {
    return next(e)
  }
};

const deleteOneBy = async (req, res, next) => {
  try {
    const resourceService = new ResourceService()
    const options = {
      where: { id: req.params.id }
    }
    const result = await resourceService.deleteBy(options, req.logged.accountId);

    return res.send({
      code: 200,
      status: 'OK',
      data: result
    });
  } catch (e) {
    return next(e)
  }
};

const updateOneBy = async (req, res, next) => {
  try {
    const resourceService = new ResourceService()
    const options = {
      where: { id: req.params.id }
    }
    const result = await resourceService.updateBy(req.body, options, req.logged.accountId);

    return res.send({
      code: 200,
      status: 'OK',
      data: result
    });
  } catch (e) {
    return next(e)
  }
};

module.exports = {
  create,
  getOneBy,
  getBy,
  deleteOneBy,
  updateOneBy
};
