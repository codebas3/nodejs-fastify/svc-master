const qsToSQL = require('../../../util/qs-to-sql');
const { v1: { RoleModule: RoleModuleService } } = require('../../../service');

const create = async (req, res, next) => {
  try {
    const roleModuleService = new RoleModuleService()
    const result = await roleModuleService.create(req.body, null, req.logged.accountId);

    return res.send({
      code: 200,
      status: 'OK',
      data: [
        { ...result.dataValues }
      ]
    });
  } catch (e) {
    return next(e)
  }
};

const getOneBy = async (req, res, next) => {
  try {
    const roleModuleService = new RoleModuleService()
    const options = {
      where: { id: req.params.id },
      count: true
    }
    const result = await roleModuleService.getBy(options);

    return res.send({
      code: 200,
      status: 'OK',
      data: result
    });
  } catch (e) {
    return next(e)
  }
};

const getBy = async (req, res, next) => {
  try {
    const roleModuleService = new RoleModuleService()
    const options = {
      ...qsToSQL.build(req.query),
      count: true
    }
    const result = await roleModuleService.getBy(options);
    return res.send({
      code: 200,
      status: 'OK',
      data: result
    });
  } catch (e) {
    return next(e)
  }
};

const deleteOneBy = async (req, res, next) => {
  try {
    const roleModuleService = new RoleModuleService()
    const options = {
      where: { id: req.params.id }
    }
    const result = await roleModuleService.deleteBy(options, req.logged.accountId);

    return res.send({
      code: 200,
      status: 'OK',
      data: result
    });
  } catch (e) {
    return next(e)
  }
};

const updateOneBy = async (req, res, next) => {
  try {
    const roleModuleService = new RoleModuleService()
    const options = {
      where: { id: req.params.id }
    }
    const result = await roleModuleService.updateBy(req.body, options, req.logged.accountId);

    return res.send({
      code: 200,
      status: 'OK',
      data: result
    });
  } catch (e) {
    return next(e)
  }
};

module.exports = {
  create,
  getOneBy,
  getBy,
  deleteOneBy,
  updateOneBy
};
