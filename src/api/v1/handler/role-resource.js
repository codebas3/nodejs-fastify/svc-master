const qsToSQL = require('../../../util/qs-to-sql');
const { v1: { RoleResource: RoleResourceService } } = require('../../../service');

const create = async (req, res, next) => {
  try {
    const roleResourceService = new RoleResourceService()
    const result = await roleResourceService.create(req.body, null, req.logged.accountId);

    return res.send({
      code: 200,
      status: 'OK',
      data: [
        { ...result.dataValues }
      ]
    });
  } catch (e) {
    return next(e)
  }
};

const getOneBy = async (req, res, next) => {
  try {
    const roleResourceService = new RoleResourceService()
    const options = {
      where: { id: req.params.id },
      count: true
    }
    const result = await roleResourceService.getBy(options);

    return res.send({
      code: 200,
      status: 'OK',
      data: result
    });
  } catch (e) {
    return next(e)
  }
};

const getBy = async (req, res, next) => {
  try {
    const roleResourceService = new RoleResourceService()
    const options = {
      ...qsToSQL.build(req.query),
      count: true
    }
    const result = await roleResourceService.getBy(options);
    return res.send({
      code: 200,
      status: 'OK',
      data: result
    });
  } catch (e) {
    return next(e)
  }
};

const deleteOneBy = async (req, res, next) => {
  try {
    const roleResourceService = new RoleResourceService()
    const options = {
      where: { id: req.params.id }
    }
    const result = await roleResourceService.deleteBy(options, req.logged.accountId);

    return res.send({
      code: 200,
      status: 'OK',
      data: result
    });
  } catch (e) {
    return next(e)
  }
};

const updateOneBy = async (req, res, next) => {
  try {
    const roleResourceService = new RoleResourceService()
    const options = {
      where: { id: req.params.id }
    }
    const result = await roleResourceService.updateBy(req.body, options, req.logged.accountId);

    return res.send({
      code: 200,
      status: 'OK',
      data: result
    });
  } catch (e) {
    return next(e)
  }
};

module.exports = {
  create,
  getOneBy,
  getBy,
  deleteOneBy,
  updateOneBy
};
