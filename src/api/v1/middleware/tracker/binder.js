const logger = require('../../../../logger')

module.exports = (req, res, next) => {
  const correlation = logger.lib.correlation('http');
  correlation.bindEmitter(req)
  correlation.bindEmitter(res)
  correlation.bindEmitter(req.socket)
  next()
};
