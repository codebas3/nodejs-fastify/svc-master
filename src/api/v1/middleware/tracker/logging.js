const { constants: { MESSAGE } } = require('../../../../config')
const logger = require('../../../../logger/http')
const { correlation: _correlation } = require('../../../../logger/lib')

const correlation = _correlation('http')

const request = (req, res, next) => {
  const useCorrelationId = req.headers['x-correlation-id'];
  
  correlation.withId(() => {
    const {
      method,
      url,
      headers: _headers,
      params,
      query,
      body
    } = req
  
    const headers = {}
    const allowedHeaders = [
      'ip',
      'host',
      'authorization',
      'content-type',
      'content-length',
      'x-correlation-id'
    ];
    allowedHeaders.forEach(h => {
      if (_headers[h]) headers[h] = _headers[h]
    });
  
    const result = { method, url, headers };
    if (params) result.params = params
    if (query) result.query = query
    if (body) result.body = body
  
    logger.info({
      msg: {
        type: MESSAGE.HTTP_INPUT,
        ...result
      }
    });

    res.header('x-correlation-id', correlation.getId());
  
    next()
  }, useCorrelationId);
}

const response =  (req, res, next) => {
  const headers = {}
  const allowedHeaders = [
    'content-type',
    'content-length'
  ];
  allowedHeaders.forEach(h => {
    const headerVal = res.getHeader(h);
    if (h) headers[h] = headerVal;
  });

  logger.info({
    msg: {
      type: MESSAGE.HTTP_OUTPUT,
      method: res.method,
      url: res.url,
      headers,
      status: res.statusCode,
      responseTime: Math.floor(res.getResponseTime() * 1000) / 1000
    }
  });
  
  next()
}

module.exports = { request, response }
