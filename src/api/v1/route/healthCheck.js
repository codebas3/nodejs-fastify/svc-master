const { HealthCheck: Handler } = require('../handler');
const { HealthCheck: Schema } = require('../schema')

const definition = {
  name: 'HealthCheck',
  basePath: '/api/v1/healthcheck',
  description: 'HealthCheck APIs v1',
  routes: [
    {
      enabled: true,
      method: 'get',
      path: '/',
      summary: 'List of service',
      description: 'Get services statuses',
      action: [
        Handler.checkAll
      ],
      responseExamples: [
        {
          code: 200,
          description: 'OK: HealthCheck',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    }
  ]
}

module.exports = definition;
