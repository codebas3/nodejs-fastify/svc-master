module.exports = {
  HealthCheck: require('./healthCheck'),
  Enumeration: require('./enumeration'),
  Role: require('./role'),
  Resource: require('./resource'),
  RoleResource: require('./role-resource'),
  Module: require('./module'),
  RoleModule: require('./role-module'),
};
