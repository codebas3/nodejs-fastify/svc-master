const { Module: Handler } = require('../handler');
const { authentication, authorization, validation } = require('../middleware');
const { Module: Schema, Common: CommonSchema } = require('../schema')

const definition = {
  name: 'Module',
  basePath: '/api/v1/module',
  description: 'Module APIs v1',
  routes: [
    {
      enabled: true,
      method: 'post',
      path: '/',
      summary: 'Add new module',
      description: 'Create new module data',
      scopes: ['module:create'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.body, 'body'),
        Handler.create
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Module',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/:id',
      summary: 'Get a module',
      description: 'Get module by id',
      scopes: ['module:read'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.params, 'params'),
        Handler.getOneBy
      ],
      validators: {
        // ? Why #ModuleParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Module',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/',
      summary: 'List of modules',
      description: 'Get multiple modules',
      scopes: ['module:read'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(CommonSchema.Query.schema, 'query'),
        Handler.getBy
      ],
      validators: {
        query: CommonSchema.Query.schema
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Module',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'delete',
      path: '/:id',
      summary: 'Delete a module',
      description: 'Delete a module by id',
      scopes: ['module:delete'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.params, 'params'),
        Handler.deleteOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #ModuleParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Module',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'put',
      path: '/:id',
      summary: 'Update a module',
      description: 'Update a module by id',
      scopes: ['module:update'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.params, 'params'),
        validation(Schema.body, 'body'),
        Handler.updateOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #ModuleParams Schema didn't appear in Swagger UI
        params: Schema.params,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Module',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
  ]
}

module.exports = definition;
