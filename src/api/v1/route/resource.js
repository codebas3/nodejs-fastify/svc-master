const { Resource: Handler } = require('../handler');
const { authentication, authorization, validation } = require('../middleware');
const { Resource: Schema, Common: CommonSchema } = require('../schema')

const definition = {
  name: 'Resource',
  basePath: '/api/v1/resource',
  description: 'Resource APIs v1',
  routes: [
    {
      enabled: true,
      method: 'post',
      path: '/',
      summary: 'Add new resource',
      description: 'Create new resource data',
      scopes: ['resource:create'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.body, 'body'),
        Handler.create
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Resource',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/:id',
      summary: 'Get a resource',
      description: 'Get resource by id',
      scopes: ['resource:read'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.params, 'params'),
        Handler.getOneBy
      ],
      validators: {
        // ? Why #ResourceParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Resource',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/',
      summary: 'List of resources',
      description: 'Get multiple resources',
      scopes: ['resource:read'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(CommonSchema.Query.schema, 'query'),
        Handler.getBy
      ],
      validators: {
        query: CommonSchema.Query.schema
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Resource',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'delete',
      path: '/:id',
      summary: 'Delete a resource',
      description: 'Delete a resource by id',
      scopes: ['resource:delete'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.params, 'params'),
        Handler.deleteOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #ResourceParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Resource',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'put',
      path: '/:id',
      summary: 'Update a resource',
      description: 'Update a resource by id',
      scopes: ['resource:update'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.params, 'params'),
        validation(Schema.body, 'body'),
        Handler.updateOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #ResourceParams Schema didn't appear in Swagger UI
        params: Schema.params,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Resource',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
  ]
}

module.exports = definition;
