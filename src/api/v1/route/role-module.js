const { RoleModule: Handler } = require('../handler');
const { authentication, authorization, validation } = require('../middleware');
const { RoleModule: Schema, Common: CommonSchema } = require('../schema')

const definition = {
  name: 'RoleModule',
  basePath: '/api/v1/role-module',
  description: 'RoleModule APIs v1',
  routes: [
    {
      enabled: true,
      method: 'post',
      path: '/',
      summary: 'Add new role-module',
      description: 'Create new role-module data',
      scopes: ['role_module:create'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.body, 'body'),
        Handler.create
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: RoleModule',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/:id',
      summary: 'Get a role-module',
      description: 'Get role-module by id',
      scopes: ['role_module:read'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.params, 'params'),
        Handler.getOneBy
      ],
      validators: {
        // ? Why #RoleModuleParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: RoleModule',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/',
      summary: 'List of role-modules',
      description: 'Get multiple role-modules',
      scopes: ['role_module:read'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(CommonSchema.Query.schema, 'query'),
        Handler.getBy
      ],
      validators: {
        query: CommonSchema.Query.schema
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: RoleModule',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'delete',
      path: '/:id',
      summary: 'Delete a role-module',
      description: 'Delete a role-module by id',
      scopes: ['role_module:delete'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.params, 'params'),
        Handler.deleteOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #RoleModuleParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: RoleModule',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'put',
      path: '/:id',
      summary: 'Update a role-module',
      description: 'Update a role-module by id',
      scopes: ['role_module:update'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.params, 'params'),
        validation(Schema.body, 'body'),
        Handler.updateOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #RoleModuleParams Schema didn't appear in Swagger UI
        params: Schema.params,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: RoleModule',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
  ]
}

module.exports = definition;
