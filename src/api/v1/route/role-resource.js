const { RoleResource: Handler } = require('../handler');
const { authentication, authorization, validation } = require('../middleware');
const { RoleResource: Schema, Common: CommonSchema } = require('../schema')

const definition = {
  name: 'RoleResource',
  basePath: '/api/v1/role-resource',
  description: 'RoleResource APIs v1',
  routes: [
    {
      enabled: true,
      method: 'post',
      path: '/',
      summary: 'Add new role-resource',
      description: 'Create new role-resource data',
      scopes: ['role_resource:create'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.body, 'body'),
        Handler.create
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: RoleResource',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/:id',
      summary: 'Get a role-resource',
      description: 'Get role-resource by id',
      scopes: ['role_resource:read'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.params, 'params'),
        Handler.getOneBy
      ],
      validators: {
        // ? Why #RoleResourceParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: RoleResource',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/',
      summary: 'List of role-resources',
      description: 'Get multiple role-resources',
      scopes: ['role_resource:read'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(CommonSchema.Query.schema, 'query'),
        Handler.getBy
      ],
      validators: {
        query: CommonSchema.Query.schema
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: RoleResource',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'delete',
      path: '/:id',
      summary: 'Delete a role-resource',
      description: 'Delete a role-resource by id',
      scopes: ['role_resource:delete'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.params, 'params'),
        Handler.deleteOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #RoleResourceParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: RoleResource',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'put',
      path: '/:id',
      summary: 'Update a role-resource',
      description: 'Update a role-resource by id',
      scopes: ['role_resource:update'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.params, 'params'),
        validation(Schema.body, 'body'),
        Handler.updateOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #RoleResourceParams Schema didn't appear in Swagger UI
        params: Schema.params,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: RoleResource',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
  ]
}

module.exports = definition;
