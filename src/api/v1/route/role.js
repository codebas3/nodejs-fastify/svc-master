const { Role: Handler } = require('../handler');
const { authentication, authorization, validation } = require('../middleware');
const { Role: Schema, Common: CommonSchema } = require('../schema')

const definition = {
  name: 'Role',
  basePath: '/api/v1/role',
  description: 'Role APIs v1',
  routes: [
    {
      method: 'post',
      path: '/',
      summary: 'Add new role',
      description: 'Create new role data',
      scopes: ['role:create'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.body, 'body'),
        Handler.create
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Role',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      method: 'get',
      path: '/:id',
      summary: 'Get a role',
      description: 'Get role by id',
      scopes: ['role:read'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.params, 'params'),
        Handler.getOneBy
      ],
      validators: {
        // ? Why #RoleParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Role',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      method: 'get',
      path: '/',
      summary: 'List of roles',
      description: 'Get multiple roles',
      scopes: ['role:read'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(CommonSchema.Query.schema, 'query'),
        Handler.getBy
      ],
      validators: {
        query: CommonSchema.Query.schema
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Role',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      method: 'delete',
      path: '/:id',
      summary: 'Delete a role',
      description: 'Delete a role by id',
      scopes: ['role:delete'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.params, 'params'),
        Handler.deleteOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #RoleParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Role',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      method: 'put',
      path: '/:id',
      summary: 'Update a role',
      description: 'Update a role by id',
      scopes: ['role:update'],
      action: [
        validation(CommonSchema.Header.schema, 'headers'),
        authentication,
        authorization,
        validation(Schema.params, 'params'),
        validation(Schema.body, 'body'),
        Handler.updateOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #RoleParams Schema didn't appear in Swagger UI
        params: Schema.params,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Role',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
  ]
}

module.exports = definition;
