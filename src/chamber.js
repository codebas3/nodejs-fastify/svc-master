const Singleton = require('./util/singleton');

class Chamber extends Singleton {
  sync(prop, data) {
    if (data) {
      this[prop] = this[prop] || new data.constructor();
      Object.assign(this[prop], data);
    }
  }
}

module.exports = Chamber;
