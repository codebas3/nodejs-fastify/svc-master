require('dotenv').config()

const { env } = process
const {
  name, description, codename, version
} = require('../../package.json')

module.exports = {
  project: env.PROJECT_NAME || env.PROJECT_NAME,
  name: env.MASTER_APP_NAME || env.APP_NAME,
  port: env.MASTER_HTTP_PORT || env.HTTP_PORT,
  codename,
  packageName: name,
  version,
  description,
  logLevel: env.MASTER_LOG_LEVEL || env.LOG_LEVEL || 'debug'
}
