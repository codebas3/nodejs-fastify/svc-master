const TYPE_INFO = {
  CONSOLE: 'Console info',
  SYSTEM: 'System info',
  REDIS: 'Redis info',
  SQL: 'SQL info',
  MONGODB: 'MongoDB info',
  CRON: 'Cron info',
  RABBITMQ: 'RabbitMQ info',
  SOCKET_IO: 'SocketIO info',
  REST_API: 'API info',
  SERVICE_V1: 'ServiceV1 info',
  GRPC: 'gRPC info',
  GRPC_CLIENT: 'gRPC client info',
  HTTP_ACCESS: 'Http access info',
  HEALTH_CHECK: 'Health check info'
}

const TYPE_WARN = {
  CONSOLE: 'Console warning',
  SYSTEM: 'System warning',
  REDIS: 'Redis warning',
  SQL: 'SQL warning',
  MONGODB: 'MongoDB warning',
  CRON: 'Cron warning',
  RABBITMQ: 'RabbitMQ warning',
  SOCKET_IO: 'SocketIO warning',
  REST_API: 'API warning',
  SERVICE_V1: 'ServiceV1 warning',
  GRPC: 'gRPC warning',
  GRPC_CLIENT: 'gRPC client warning',
  HTTP_ACCESS: 'Http access warning',
  HEALTH_CHECK: 'Health check warning'
}

const TYPE_ERROR = {
  UNCAUGHT: 'Uncaught exception!',
  CONSOLE: 'Console error',
  SYSTEM: 'System error',
  REDIS: 'Redis error',
  SQL: 'SQL error',
  MONGODB: 'MongoDB error',
  CRON: 'Cron error',
  RABBITMQ: 'RabbitMQ error',
  SOCKET_IO: 'SocketIO error',
  REST_API: 'API error',
  SERVICE_V1: 'ServiceV1 error',
  GRPC_CLIENT: 'gRPC client error',
  HTTP_ACCESS: 'Http access error',
  HEALTH_CHECK: 'Health check error',
  AUTHENTICATION: 'Authentication error',
  AUTHORIZATION: 'Authorization error'
}

const MESSAGE = {
  /** listening port message */
  HTTP_LISTENED: 'HTTP listening on port',
  HTTP_STOPPED: 'HTTP stopped from port',
  HTTP_INPUT: 'Incoming request',
  HTTP_OUTPUT: 'Outgoing response',
  GRPC_LISTENED: 'gRPC listening on port',
  GRPC_STOPPED: 'gRPC stopped from port',
  GRPC_FORCE_STARTED: 'gRPC force to start!',
  GRPC_FORCE_STOPPED: 'gRPC force to stop!',
  SOCKET_IO_LISTENED: 'SocketIO listening on port',
  SOCKET_IO_STOPPED: 'SocketIO stopped from port',
  SOCKET_IO_NAMESCPACE_CREATED: 'SocketIO created namespace',

  /** sql message */
  SQL_CONNECTED: 'SQL database connected',
  SQL_DISCONNECTED: 'SQL database disconnected',
  SQL_POOL_CONNECTING: 'SQL pool on connecting',
  SQL_POOL_CONNECTED: 'SQL pool has connected',
  SQL_POOL_RELEASING: 'SQL pool on releasing',
  SQL_POOL_RELEASED: 'SQL pool has released',

  /** mongodb message */
  MONGODB_CONNECTED: 'MongoDB database connected',
  MONGODB_DISCONNECTED: 'MongoDB database disconnected',
  MONGODB_POOL_CONNECTING: 'MongoDB pool on connecting',
  MONGODB_POOL_CONNECTED: 'MongoDB pool has connected',
  MONGODB_POOL_DISCONNECTING: 'MongoDB pool on disconnecting',
  MONGODB_POOL_DISCONNECTED: 'MongoDB pool has disconnected',
  MONGODB_POOL_CLOSED: 'MongoDB pool closed',
  MONGODB_POOL_RECONNECTED: 'MongoDB pool has reconnected',
  MONGODB_POOL_RECONNECT_FAIL: 'MongoDB pool fail to reconnect',
  MONGODB_POOL_ERROR: 'MongoDB pool has error',
  MONGODB_POOL_FULLSETUP: 'MongoDB pool has fullsetup to replica set',
  MONGODB_POOL_ALL_CONNECTED: 'MongoDB pool has connected to all connections',

  /** redis message */
  REDIS_CONNECTED: 'Redis client connected',
  REDIS_NOT_CONNECTED: 'Redis client disconnected',
  REDIS_READY_TO_USE: 'Redis ready to use',

  /** rabbit mq message */
  RABBITMQ_NOT_CONNECTED: 'RabbitMQ client disconnected',
  RABBITMQ_CONNECTION_CREATION: 'RabbitMQ connection creation',
  RABBITMQ_CONNECTION_ERROR: 'RabbitMQ connection error',
  RABBITMQ_CONNECTION_CLOSED: 'RabbitMQ connection closed',
  RABBITMQ_CONNECTION_ESTABLISH: 'RabbitMQ connection establish',
  RABBITMQ_CHANNEL_CREATION: 'RabbitMQ channel creation',
  RABBITMQ_CHANNEL_ERROR: 'RabbitMQ channel error',
  RABBITMQ_CHANNEL_CLOSED: 'RabbitMQ channel closed',
  RABBITMQ_CHANNEL_ESTABLISH: 'RabbitMQ channel establish',
  RABBITMQ_PUBLISH: 'RabbitMQ publishing',
  RABBITMQ_PUBLISH_OK: 'RabbitMQ succeeded publishing',
  RABBITMQ_PUBLISH_FALLBACK: 'RabbitMQ publish got fallback',
  RABBITMQ_CONSUME: 'RabbitMQ consuming',
  RABBITMQ_CONSUME_OK: 'RabbitMQ succeeded consuming',
  RABBITMQ_CONSUME_FALLBACK: 'RabbitMQ failed consuming',
  RABBITMQ_NOACK: 'RabbitMQ NOACK a message',

  /** common message */
  ERROR_REQUEST_DATA: 'Error request data',
  SYSTEM_ERROR: 'Internal system error',
  REQUEST_DATA_ERROR: 'There is a problem with the request data',
  RESPONSE_DATA_ERROR: 'There is a problem with the response data',

  /** socket message */
  SOCKET_IO_DISCONNECTED: 'Socket client disconnected',
  SOCKET_IO_CONNECTED: 'Socket client connected',
  SOCKET_IO_ERROR: 'Socket client error',
  SOCKET_IO_ROOM_NOT_EXIST: 'Room doesn\'t exist',
  SOCKET_IO_TOKEN_EXPIRED: 'Socket token is expired',
  SOCKET_IO_UNABLE_TO_INIT_ROOM: 'Unable to init room',
  SOCKET_IO_UNABLE_TO_JOIN_ROOM: 'Unable to join room',
}

module.exports = {
  TYPE_INFO,
  TYPE_WARN,
  TYPE_ERROR,
  MESSAGE
};
