require('dotenv').config()

const { env } = process

module.exports = {
  cronTime: env.MASTER_CRON_TIME || env.CRON_TIME || '0 0 * * * *'
}
