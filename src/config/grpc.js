require('dotenv').config()

const { env } = process

module.exports = {
  port: env.MASTER_GRPC_PORT || env.GRPC_PORT,
  forever: true,
  serviceOpts: {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
  }
}
