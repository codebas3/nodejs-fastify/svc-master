require('dotenv').config();

const { env } = process;

module.exports = {
  svc: {
    master: env.MASTER_SVC_URL || env.SVC_URL,
    oauth: env.OAUTH_SVC_URL || env.OAUTH_SVC_URL
  },
  rpc: {
    master: env.MASTER_RPC_URL || env.RPC_URL,
    oauth: env.OAUTH_RPC_URL || env.OAUTH_RPC_URL
  }
}
