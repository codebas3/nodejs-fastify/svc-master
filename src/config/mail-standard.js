require('dotenv').config();

const { env } = process
const port = parseInt(env.MASTER_MAIL_PORT || env.MAIL_PORT)
const receiver = env.MASTER_MAIL_RECEIVER || env.MAIL_RECEIVER
const sender = env.MASTER_MAIL_SENDER || env.MAIL_SENDER

module.exports = {
  sender: {
    name: env.MASTER_MAIL_SENDER_NAME || env.MAIL_SENDER_NAME,
    email: sender
  },
  receiver: {
    email: receiver || sender
  },
  transport: {
    host: env.MASTER_MAIL_HOST || env.MAIL_HOST,
    port,
    secure: port === 465,
    auth: {
      user: env.MASTER_MAIL_USERNAME || env.MAIL_USERNAME,
      pass: env.MASTER_MAIL_PASSWORD || env.MAIL_PASSWORD
    },
    tls: { rejectUnauthorized: false },
    debug: true
  }
}
