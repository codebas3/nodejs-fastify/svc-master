require('dotenv').config()

const { env } = process

const config = {
  username: env.MASTER_MONGO_USERNAME || env.MONGO_USERNAME,
  password: env.MASTER_MONGO_PASSWORD || env.MONGO_PASSWORD,
  database: env.MASTER_MONGO_DBNAME || env.MONGO_DBNAME,
  host: env.MASTER_MONGO_HOST || env.MONGO_HOST,
  port: env.MASTER_MONGO_PORT || env.MONGO_PORT,
  pool: {
    bufferCommands: true,
    minPoolSize: 0,
    maxPoolSize: 5,
    connectTimeoutMS: 30000, // default: 30000
    socketTimeoutMS: 30000, // default: 30000
    serverSelectionTimeoutMS: 30,
    heartbeatFrequencyMS: 1000
  },
  family: 4,
  autoIndex: false,
  logConnection: true, // env.NODE_ENV !== 'production'
}

if (env.NODE_ENV === 'production') {
  config.logging = false
}

module.exports = config
