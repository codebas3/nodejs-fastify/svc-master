require('dotenv').config()

const { env } = process

module.exports = {
  port: env.MASTER_RABBITMQ_PORT || env.RABBITMQ_PORT,
  host: env.MASTER_RABBITMQ_HOST || env.RABBITMQ_HOST,
  user: env.MASTER_RABBITMQ_USERNAME || env.RABBITMQ_USERNAME,
  password: env.MASTER_RABBITMQ_PASSWORD || env.RABBITMQ_PASSWORD,
  queue: {
    Enum: {
      exchange: {
        name: 'enumeration',
        type: 'topic',
        options: {
          durable: true
        }
      },
      route: 'enumeration',
      // For service cluster which mean "svc-this" has multiple instances
      // Scenario 1: You should add more prefix if you need every instance of subscription/consumer receive the message
      // Scenario 2: Don't add more prefix if your service cluster just want to receive one only from many (of same instance)
      queue: 'svc-master:enumeration'
    },
    Role: {
      exchange: {
        name: 'role',
        type: 'topic',
        options: {
          durable: true
        }
      },
      route: 'role',
      queue: 'svc-master:role'
    },
    RoleResource: {
      exchange: {
        name: 'role-resource',
        type: 'topic',
        options: {
          durable: true
        }
      },
      route: 'role-resource',
      queue: 'svc-master:role-resource'
    }
  }
};
