require('dotenv').config();

const { env } = process

module.exports = {
  host: env.MASTER_REDIS_HOST || env.REDIS_HOST,
  port: env.MASTER_REDIS_PORT || env.REDIS_PORT,
  family: 4, // 4 (IPv4) or 6 (IPv6)
  db: env.MASTER_REDIS_DBNAME || env.REDIS_DBNAME || 0,
  username: env.MASTER_REDIS_USERNAME || env.REDIS_USERNAME,
  password: env.MASTER_REDIS_PASSWORD || env.REDIS_PASSWORD,
}
