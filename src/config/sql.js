require('dotenv').config()

const { env } = process
const isProd = env.NODE_ENV === 'production'
const fs = require('fs')

const config = {
  username: env.MASTER_SQL_USERNAME || env.SQL_USERNAME,
  password: env.MASTER_SQL_PASSWORD || env.SQL_PASSWORD,
  database: env.MASTER_SQL_DBNAME || env.SQL_DBNAME,
  host: env.MASTER_SQL_HOST || env.SQL_HOST,
  port: env.MASTER_SQL_PORT || env.SQL_PORT,
  dialect: env.MASTER_SQL_DIALECT || env.SQL_DIALECT,
  pool: {
    // * If time has reached, will given an error:
    // * ConnectionAcquireTimeoutError [SequelizeConnectionAcquireTimeoutError]: Operation timeout
    min: 0, // default: 0
    max: 5, // default: 5
    idle: 5000, // default: 10000
    acquire: 60000 // default: 60000
  },
  dialectOptions: {
    encoding: env.MASTER_SQL_ENCODING || env.SQL_ENCODING,
    charset: env.MASTER_SQL_CHARSET || env.SQL_CHARSET,
    collate: env.MASTER_SQL_COLLATE || env.SQL_COLLATE,
    supportBigNumbers: true,
    bigNumberStrings: true,
    options: {
      // * Non MSSQL dialect will force to ignore all of these options
      // * If time has reached, will given an error:
      // * DatabaseError [SequelizeDatabaseError]: Timeout: Request failed to complete in $requestTimeout ms
      cancelTimeout: 5000, // default: 5000
      requestTimeout: 15000 // default: 15000
    }
  },
  logging: isProd,
  logConnection: true,
  logQueryParameters: !isProd,
  migrationStorage: 'sequelize',
  migrationStorageTableName: '_mstrMetas',
  seederStorage: 'sequelize',
  seederStorageTableName: '_mstrSeeds'
}

if (config.dialect === 'mssql') {
  config.dialectOptions.options = {
    ...config.dialectOptions.options,
    enableArithAbort: true
  }
} else if (config.dialect === 'mysql') {
  delete config.dialectOptions.options
  delete config.dialectOptions.collate
} else if (config.dialect === 'postgres') {
  delete config.dialectOptions
}

if ((env.MASTER_SQL_SSL_MODE || env.SQL_SSL_MODE) === 'require') {
  config.dialectOptions.ssl = {
    require: true,
    rejectUnauthorized: false
  };
}

const sslKey = env.MASTER_SQL_SSLKEY || env.SQL_SSL_KEY

try {
  const cert = fs.readFileSync(sslKey);
  config.dialectOptions.ssl = {
    require: true,
    rejectUnauthorized: false,
    ca: [cert]
  };
} catch (e) {
  if (env.sslKey) {
    console.error(`SQL_SSLKEY=${sslKey} key doesn't found!`)
    console.error(e)
    process.exit()
  }
}

module.exports = config
