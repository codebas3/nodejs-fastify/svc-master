module.exports = {
  routePrefix: '/docs',
  uiConfig: {
    docExpansion: 'full',
    deepLinking: true,
    onComplete: function () {
      // you will see this in browser
      console.info('welcome to swagger ui')
    }
  },
  uiHooks: {
    onRequest: function (request, reply, next) { next() },
    preHandler: function (request, reply, next) { next() }
  },
  staticCSP: true,
  transformStaticCSP: (header) => header,
  transformSpecification: (swaggerObject, req, res) => {
    swaggerObject.host = req.hostname
    return swaggerObject
  },
  transformSpecificationClone: true
}
