const { app: { name, logLevel } } = require('../config');
const { logger: _logger, stack, correlation: _correlation } = require('./lib')

const correlation = _correlation('http')
const logger = _logger({
  name,
  level: logLevel,
  correlation,
  caller: stack(2)
})

module.exports = logger
