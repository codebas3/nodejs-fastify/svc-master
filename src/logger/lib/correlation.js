const cls = require('cls-hooked');
const uuid = require('uuid');

const storage = {};

module.exports = (namespace = 'none') => {
  if (storage[namespace]) return storage[namespace]

  const store = cls.createNamespace(namespace);

  function withId(callback, correlationId) {
    return store.run(() => {
      store.set('correlationId', correlationId || uuid.v4())
      if (callback) callback()
    });
  }

  function getId() {
    return store.get('correlationId');
  }

  storage[namespace] = {
    namespace,
    withId,
    getId,
    bindEmitter: store.bindEmitter.bind(store),
    bind: store.bind.bind(store)
  }

  return storage[namespace];
}
