const { message: Err } = require('../../../src/config');
const Slimy = require('../../util/slimy');

const slimy = Slimy.getInstance();
// slimy.SEPARATOR = '[REDUCTED]';

const stringify = (type, message, reference) => {
  return {
    msg: {
      type,
      reference,
      message: slimy.reduction(message)
    }
  }
};

const objError = (error) => {
  const {
    code,
    message,
    stack,
    isCustom
  } = error;

  return {
    code,
    reason: (message || '').split('\n').length > 1 ? message.split('\n') : message,
    stack: (stack || '').split('\n').length > 1 ? stack.split('\n') : stack,
    isCustom
  }
};

const newError = (key, lang, params) => {
  const { message, code } = Err.get(key, lang, params)
  const error = new Error(message);
  error.code = code;
  error.isCustom = true;

  return error
};

module.exports = {
  stringify,
  objError,
  newError
};
