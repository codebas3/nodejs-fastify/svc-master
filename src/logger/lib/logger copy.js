const winston = require('winston');

const { app: { name } } = require('../../config');

module.exports = function (opts = {}) {
  const {
    level = 'info',
    correlation,
    noCorrelationIdValue = 'nocorrelation',
    caller
  } = opts;

  return winston.createLogger({
    format: winston.format.combine(
      winston.format((info) => {
        info.namespace = correlation.namespace;
        info.correlationId = correlation.getId() || noCorrelationIdValue;
        info.caller = caller || 'console';

        return info;
      })(),
      winston.format.timestamp(),
      winston.format.errors({ stack: true }),
      winston.format.colorize(),
      winston.format.printf(({
        namespace,
        correlationId,
        timestamp,
        level,
        caller,
        message
      }) => `${name} ${namespace} ${timestamp} ${level} ${correlationId} (${caller}): ${message}`)
    ),
    level,
    transports: [
      new winston.transports.Console({
        handleExceptions: false
      })
    ],
    exitOnError: true
  });
};
