const Pino = require('pino');

const { app: { name } } = require('../../config');

module.exports = function (opts = {}) {
  const {
    level = 'info',
    correlation,
    noCorrelationIdValue = 'nocorrelation',
    caller
  } = opts;

  return Pino({
    level,
    formatters: {
      log(object) {
        Object.assign(object, {
          service: name,
          namespace: correlation.namespace,
          correlationId: correlation.getId() || noCorrelationIdValue,
          caller: caller || 'console'
        })
        
        return object
      },
      level(label) {
        return { level: label.toUpperCase() };
      },
    },
    timestamp: Pino.stdTimeFunctions.isoTime,
  });
};
