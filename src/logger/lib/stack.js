
const { basePath } = require('../../config');

module.exports = (index) => {
  // * This is fake error for getting where the method call from;
  // * in case, we can't use arguments.caller in strict mode
  const e = new Error();
  let caller = e.stack.split('\n');
  caller = caller[index].trim().split(/\s/g).slice(-1)[0]
  caller = caller.substring(1, caller.length - 1).replace(basePath, '').substring(1);

  return caller
}