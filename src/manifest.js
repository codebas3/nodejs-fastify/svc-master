const fs = require('fs')
const path = require('path')
const Promise = require('bluebird')
const { convert } = require('joi-route-to-swagger')
const { faker } = require('@faker-js/faker');

const logger = require('./logger/console')(__filename);
const { stringify } = require('./logger/lib/format');
const { constants, app, env } = require('./config');
const Singleton = require('./util/singleton');
const { Redis } = require('./cache');
const Sql = require('./model/sql');
const SqlRepo = require('./repository/sql');
const Mongo = require('./model/mongo');
const MongoRepo = require('./repository/mongo');
const Server = require('./server');
// const { Client: RMQ } = require('./queue/rabbitmq');
// const { Enumeration, Role, RoleResource } = require('./queue-consumer');
// const GRPC = require('./rpc/grpc');
// const IO = require('./io/io');
const Cron = require('./cron');

const { TYPE_ERROR, MESSAGE } = constants;

class Manifest extends Singleton {
  async setup() {
    await this.setRedis()
    await this.setSQL()
    await this.setMongoDB()
    // await this.setRabbitMQ()
    await this.setCron()
    await this.setServer()
    // await this.setGRPC()
    // await this.setIO()

    return this;
  }

  async disconnect() {
    await this.redis.disconnect();
    await this.mongo.disconnect();
    await this.sql.disconnect();
  }

  async stop() {
    await this.cron.stopAll();
    await this.grpc.stop(true);
    // await this.io.stop();
    await this.server.stop();
  }

  async start() {
    this.server.start();
    // this.grpc.start();
    // this.io.start();
    // this.cron.jobTest();
    // this.setData();
  }

  async setRedis() {
    this.redis = Redis.getInstance();
    await this.redis.connect();
  }

  async setSQL() {
    this.sql = Sql.getInstance();
    await this.sql.connect();
    this.sql.repo = SqlRepo;
  }

  async setMongoDB() {
    this.mongo = Mongo.getInstance();
    await this.mongo.connect();
    this.mongo.repo = MongoRepo;
  }

  async setRabbitMQ() {
    const me = this;

    const channelId = `ch:${app.name}:${faker.word.adjective()}`

    // * reusable client
    me.rmq = RMQ.getInstance()

    // * reusable channel
    me.rmq.channel = await this.rmq.confirmChannel(channelId);

    me.rmq.channel.listeners('return').map(fn => me.rmq.channel.removeListener('return', fn))
    me.rmq.channel.on('return', ({ fields, content }) => {
      const data = { fields, content: JSON.parse(content) }
      logger.error(
        stringify(
          TYPE_ERROR.RABBIT_MQ,
          `${MESSAGE.RABBITMQ_PUBLISH_FALLBACK} (${me.rmq.channel._id}): Failed to publish`,
          data
        )
      )
    })

    // Data subscriptions
    Enumeration.subscribe(me.rmq)
    Role.subscribe(me.rmq)
    RoleResource.subscribe(me.rmq)
  }

  // eslint-disable-next-line class-methods-use-this
  async setData() {
    // Data initializers
    await Enumeration.sync(true);
    await Role.sync(true);
    await RoleResource.sync(true);
  }

  async setServer() {
    const allowedOrigins = ['*'];
    const corsOptions = {
      origin(origin, callback) {
        if (!origin) {
          /* Allow requests with no origin (like mobile apps or curl requests) */
          return callback(null, true);
        }

        if (!allowedOrigins.includes(origin)) {
          const message = 'The CORS policy for this site does not allow access from the specified origin.';
          return callback(new Error(message), false);
        }

        return callback(null, true);
      },
      optionsSuccessStatus: 200
    };

    const apiRoutes = require('./api/v1/route');
    Object.values(apiRoutes).forEach(el => {
      el.routes = el.routes.filter(route => route.enabled !== false)
    })

    const { DOCS_DEF, ROUTE_DEF } = require('./api/v1/docs');
    const swaggerDocs = convert(Object.values(apiRoutes), DOCS_DEF, ROUTE_DEF)

    const distdir = path.resolve(`${__dirname}/../dist`);
    if (!fs.existsSync(distdir)) fs.mkdirSync(distdir);
    fs.writeFileSync(
      path.join(distdir, 'swagger.json'),
      JSON.stringify(swaggerDocs, 0, 2)
    )

    const assetsdir = path.join(distdir, 'assets');
    if (!fs.existsSync(assetsdir)) fs.mkdirSync(assetsdir);

    const pagedir = path.resolve(__dirname, 'page');
    const webmanifest = JSON.parse(fs.readFileSync(
      path.join(pagedir, 'asset', 'site.webmanifest')
    ));
    fs.writeFileSync(
      path.resolve(assetsdir, 'site.webmanifest'),
      JSON.stringify({
        ...webmanifest,
        name: `${app.project}: ${app.codename}`,
        short_name: app.project
      }, 0, 2)
    )

    Server.register({
      corsOptions,
      docs: swaggerDocs,
      routes: apiRoutes
    });
    this.server = Server.getInstance();
  }

  async setGRPC() {
    this.grpc = GRPC.getInstance();
  }

  async setIO() {
    this.io = IO.getInstance();
  }

  async setCron() {
    this.cron = Cron.getInstance();
  }
}

module.exports = Manifest;
