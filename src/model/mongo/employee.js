const name = 'Employee';
const template = {
  name: String,
  email: { 
    type: String,
    index: true,
    required: true,
  }
}

module.exports = { name, template, }
