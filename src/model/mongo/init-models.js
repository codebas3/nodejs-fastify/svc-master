const Mongoose = require('mongoose');
const { unixNow } = require('../../util/timezone');
const collections = require('../../config/mongoCollections');

const { Schema } = Mongoose;

const additionalField = {
  createdAt: {
    type: Number,
    required: true,
    default: unixNow,
  },
  updatedBy: { type: String },
  updatedAt: { type: Number },
  deletedBy: { type: String },
  deletedAt: { type: Number },
  notes: { type: String }
}

// const fs = require('fs');
// const schemaOnly = name => (name.indexOf('.js') === name.length - 3) && (['index.js', 'init-models.js'].indexOf(name) === -1);
// const schemas = fs.readdirSync(__dirname).filter(schemaOnly).map(name => require(`./${name}`));
const schemas = collections.map(name => require(`./${name}`));
const initModels = (mongoose) => schemas.reduce((all, schema) => {
  if (!mongoose.models[schema.name]) {
    return {
      ...all,
      [schema.name]: mongoose.model(
        schema.name,
        new Schema(
          { ...schema.template, ...additionalField },
          { collection: schema.name.toLowerCase() }
        )
      )
    }
  }
  return false;
}, {})

module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
