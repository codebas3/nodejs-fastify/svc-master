const Employee = require('./employee');

const name = 'Presence';
const template = {
  date: {
    type: Number, // use unix timestamp for fast searching!
    required: true,
  },
  employee: { ...Employee.template },
  logs: [
    {
      state: {
        type: String, // e.g. 'in' or 'out'
        required: true,
      }
    }
  ]
}

module.exports = { name, template, }
