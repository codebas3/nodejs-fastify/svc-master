/* eslint-disable prefer-destructuring */

const _ = require('lodash')
const { Op, Sequelize } = require('sequelize')
const logger = require('../../logger/console')(__filename);
const { stringify, objError } = require('../../logger/lib/format');
const { sql: opts, constants } = require('../../config');

const { TYPE_INFO, TYPE_ERROR, MESSAGE } = constants;
const Singleton = require('../../util/singleton');
const initModels = require('./init-models');

class Sql extends Singleton {
  opts;

  model;

  sequelize;

  constructor() {
    super();
    this.opts = opts;
    this.sequelize = new Sequelize(opts);

    if ([undefined, true].indexOf(opts.logConnection) > -1) {
      // this.sequelize.beforeConnect((...args) => logger.debug(stringify(TYPE_INFO.SQL, MESSAGE.SQL_POOL_CONNECTING)))
      this.sequelize.afterConnect((...args) => logger.debug(stringify(TYPE_INFO.SQL, MESSAGE.SQL_POOL_CONNECTED)))
      // this.sequelize.beforeDisconnect((...args) => logger.debug(stringify(TYPE_INFO.SQL, MESSAGE.SQL_POOL_RELEASING)))
      this.sequelize.afterDisconnect((...args) => logger.debug(stringify(TYPE_INFO.SQL, MESSAGE.SQL_POOL_RELEASED)))
    }
  }

  async connect() {
    try {
      await this.sequelize.authenticate()
      logger.info(stringify(TYPE_INFO.SQL, MESSAGE.SQL_CONNECTED))

      this.model = initModels(this.sequelize)
    } catch (err) {
      logger.error(stringify(TYPE_ERROR.SQL, objError(err)));
      if (err.parent) {
        if (err.parent.message !== err.message) {
          logger.error(stringify(TYPE_ERROR.SQL, objError(err.parent)))
        }
      }
      if (err.original) {
        if (err.parent) {
          if (err.original.message !== err.parent.message) {
            logger.error(stringify(TYPE_ERROR.SQL, objError(err.original)))
          }
        }
      }

      await this.disconnect();
    }
  }

  async disconnect() {
    return this.sequelize.close();
  }

  async getDatetime(options) {
    const data = await this.sequelize.query(
      opts.query.dateTime,
      { ...options, raw: true, type: Sequelize.QueryTypes.SELECT }
    )
    return Object.values(data[0])[0]
  }

  fixOperator(condition) {
    const me = this;
    const data = _.cloneDeep(condition)
    Object.entries(data).forEach(([key, value]) => {
      if (value instanceof Object) data[key] = me.fixOperator(value)

      if (key.indexOf('$') === 0) {
        data[Op[key.substr(1)]] = _.cloneDeep(value)
        delete data[key]
      }
    })
    return data
  }

  constructOptions(options = {}, multiple = true) {
    options = options || {}
    if (multiple) {
      options.limit = options.limit || 10
      options.offset = options.offset || 0

      if (options.limit === -1) delete options.limit;
    }

    if (options.where) options.where = this.fixOperator(options.where)
    if (options.include) {
      if (options.attributes) {
        const attributes = []
        Object.keys(options.attributes).forEach(idx => {
          const key = options.attributes[idx]
          const keys = key.split('.')
          if (keys.length > 1) {
            const field = keys[keys.length - 1]

            keys.pop()

            const alias = keys.join('.')
            const inner = options.include.filter(el => el.as === alias)[0]
            if (inner) {
              inner.attributes = inner.attributes || []
              inner.attributes.push(field)
            } else {
              attributes.push(key)
            }
          } else {
            attributes.push(key)
          }
        })
        delete options.attributes
        if (attributes.length) options.attributes = attributes
      }

      Object.keys(options.where).forEach(field => {
        const keys = field.split('.')
        if (keys.length > 1) {
          let inner = options.include;
          for (const i in keys) {
            const isLast = keys.length - i === 1
            const key = keys[i];

            if (isLast) {
              inner.where = inner.where || {}
              inner.where[key] = options.where[field]
              inner.where.deletedAt = { [Op.is]: null }
            } else if (inner.include) {
              inner = inner.include.filter(el => el.as === key)[0]
            } else {
              inner = inner.filter(el => el.as === key)[0]
            }
          }

          delete options.where[field]
        }
      })
    }

    return options
  }
}

module.exports = Sql;
