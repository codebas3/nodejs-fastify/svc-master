const os = require('os');
const _ = require('lodash');

const Chamber = require('../../chamber')
const { Subscriber } = require('../../queue/rabbitmq');
const EnumerationClientRPC = require('../grpc-client/enumeration');
const logger = require('../../logger/console')(__filename);
const { stringify, objError } = require('../../logger/lib/format');
const correlationId = require('../../logger/cron');
const { rabbitmq: opts, constants, env } = require('../../config');

const { TYPE_ERROR, TYPE_INFO } = constants;

const sync = async (exit) => {
  const enumerationClientRPC = new EnumerationClientRPC();
  try {
    const data = await enumerationClientRPC.getAll();
    const enumerations = data.rows.reduce((o, { key, value }) => _.set(o, key, value), {});
    const chamber = Chamber.getInstance();

    chamber.sync('enums', enumerations)

    return enumerations
  } catch (err) {
    if (exit) throw err;
    else {
      logger.error(stringify(TYPE_ERROR.GRPC_CLIENT, objError(err)));
      return undefined
    }
  }
}

const subscribe = async (client, callback) => {
  const { Enum } = opts.queue;
  const queueName = `${Enum.queue}:${os.hostname()}`
  const subscriber = new Subscriber(client, Enum.exchange, client.channel)

  return subscriber.consume(queueName, Enum.route, null, async (msg) => {
    logger.info(stringify(
      `${TYPE_INFO.RABBIT_MQ}`,
      `Consuming ${queueName}: ${msg.content.toString()}`
    ));
    correlationId.storeId();

    const enumerations = await sync();
    return callback instanceof Function ? callback(enumerations) : enumerations
  }, -1);
}

module.exports = {
  sync,
  subscribe
}
