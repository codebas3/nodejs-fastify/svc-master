module.exports = {
  Enumeration: require('./enumeration'),
  Role: require('./role'),
  RoleResource: require('./role-resource')
}
