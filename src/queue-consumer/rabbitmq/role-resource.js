const os = require('os');

const Chamber = require('../../chamber')
const { Subscriber } = require('../../queue/rabbitmq');
const RoleResourceClientRPC = require('../grpc-client/role-resource');
const logger = require('../../logger/console')(__filename);
const { stringify, objError } = require('../../logger/lib/format');
const correlationId = require('../../logger/cron');
const { rabbitmq: opts, constants, env } = require('../../config');

const { TYPE_ERROR, TYPE_INFO } = constants;

const sync = async (exit) => {
  const roleResourceClientRPC = new RoleResourceClientRPC();
  try {
    const resources = await roleResourceClientRPC.getAll();
    const chamber = Chamber.getInstance();

    chamber.sync('resources', resources.rows)

    return resources;
  } catch (err) {
    if (exit) throw err
    else {
      logger.error(stringify(TYPE_ERROR.GRPC_CLIENT, objError(err)));
      return undefined
    }
  }
}

const subscribe = async (client, callback) => {
  if (env === 'test') {
    return callback instanceof Function ? callback() : null
  }

  const { RoleResource } = opts.queue;
  const queueName = `${RoleResource.queue}:${os.hostname()}`
  const subscriber = new Subscriber(client, RoleResource.exchange, client.channel)

  return subscriber.consume(queueName, RoleResource.route, null, async (msg) => {
    logger.info(stringify(
      `${TYPE_INFO.RABBIT_MQ}`,
      `Consuming ${queueName}: ${msg.content.toString()}`
    ));
    correlationId.storeId();

    const roleResources = await sync();
    return callback instanceof Function ? callback(roleResources) : roleResources
  }, -1);
}

module.exports = {
  sync,
  subscribe
}
