const os = require('os');

const Chamber = require('../../chamber')
const { Subscriber } = require('../../queue/rabbitmq');
const RoleClientRPC = require('../grpc-client/role');
const logger = require('../../logger/console')(__filename);
const { stringify, objError } = require('../../logger/lib/format');
const correlationId = require('../../logger/cron');
const { rabbitmq: opts, constants, env } = require('../../config');

const { TYPE_ERROR, TYPE_INFO } = constants;

const sync = async (exit) => {
  const roleClientRPC = new RoleClientRPC();
  try {
    const roles = await roleClientRPC.getAll();
    const chamber = Chamber.getInstance();

    chamber.sync('roles', roles.rows)

    return roles
  } catch (err) {
    if (exit) throw err
    else {
      logger.error(stringify(TYPE_ERROR.GRPC_CLIENT, objError(err)));
      return undefined
    }
  }
}

const subscribe = async (client, callback) => {
  if (env === 'test') {
    return callback instanceof Function ? callback() : null
  }

  const { Role } = opts.queue;
  const queueName = `${Role.queue}:${os.hostname()}`
  const subscriber = new Subscriber(client, Role.exchange, client.channel)

  return subscriber.consume(queueName, Role.route, null, async (msg) => {
    logger.info(stringify(
      `${TYPE_INFO.RABBIT_MQ}`,
      `Consuming ${queueName}: ${msg.content.toString()}`
    ));
    correlationId.storeId();

    const roles = await sync();
    return callback instanceof Function ? callback(roles) : roles
  }, -1);
}

module.exports = {
  sync,
  subscribe
}
