const Mongo = require('../../model/mongo')

const { mongoose } = Mongo.getInstance();

async function getOneBy(options) {
  const criteria = {
    ...options,
    where: {
      ...options.where
    },
  };
  return mongoose.models.Employee.findOne(criteria);
}

async function create(data) {
  const newData = new mongoose.models.Employee(data);
  return newData.save();
}

async function updateBy(options, data) {
  const criteria = {
    ...options,
    ...data
  };

  return mongoose.models.Employee.updateOne(criteria);
}

async function removeBy(options) {
  const criteria = {
    ...options,
    where: {
      ...options.where,
    },
  };

  return mongoose.models.Employee.deleteOne(criteria);
}

module.exports = {
  getOneBy, create, updateBy, removeBy
};
