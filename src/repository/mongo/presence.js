const Mongo = require('../../model/mongo')

const { mongoose } = Mongo.getInstance();

async function getOneBy(options) {
  const criteria = {
    ...options,
    where: {
      ...options.where
    },
  };
  return mongoose.models.Presence.findOne(criteria);
}

async function create(data) {
  const newData = new mongoose.models.Presence(data);
  return newData.save();
}

async function updateBy(options, data) {
  const criteria = {
    ...options,
    ...data
  };

  return mongoose.models.Presence.updateOne(criteria);
}

async function removeBy(options) {
  const criteria = {
    ...options,
    where: {
      ...options.where,
    },
  };

  return mongoose.models.Presence.deleteOne(criteria);
}

module.exports = {
  getOneBy, create, updateBy, removeBy
};
