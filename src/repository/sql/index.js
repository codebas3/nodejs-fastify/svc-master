module.exports = {
  enumeration: require('./enumeration'),
  role: require('./role'),
  module: require('./module'),
  resource: require('./resource'),
  roleModule: require('./role-module'),
  roleResource: require('./role-resource')
}
