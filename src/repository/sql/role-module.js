const { Op, Sequelize } = require('sequelize');
const Sql = require('../../model/sql')

const sql = Sql.getInstance();
const { sequelize } = sql

function getRelations() {
  return [
    { model: sequelize.models.mstrRoles, as: 'mstrRole' },
    { model: sequelize.models.mstrModules, as: 'mstrModule' }
  ]
}

async function create(data, options = {}, writer = null) {
  return sequelize.models.mstrRoleModules.create(
    {
      createdBy: writer || 'SYSTEM',
      createdAt: await sql.getDatetime({
        ...{ transaction: options ? options.transaction : null },
      }),
      ...data
    },
    sql.constructOptions(options, false)
  )
}

async function getBy(options = {}) {
  const count = options.count || false
  const opts = {
    ...options,
    include: getRelations(),
    where: {
      ...options.where,
      deletedAt: { [Op.is]: null }
    }
  }
  const parameters = sql.constructOptions(opts)
  if (count) return sequelize.models.mstrRoleModules.findAndCountAll(parameters)

  return sequelize.models.mstrRoleModules.findAll(parameters)
}

async function deleteBy(options = {}, writer = null) {
  const opts = {
    ...options,
    where: {
      ...options.where,
      deletedAt: { [Op.is]: null }
    },
    returning: true
  }
  const [count, rows] = await sequelize.models.mstrRoleModules.update(
    {
      deletedBy: writer || 'SYSTEM',
      deletedAt: await sql.getDatetime({
        ...{ transaction: options ? options.transaction : null },
      })
    },
    sql.constructOptions(opts, false)
  )

  return { count, rows }
}

async function updateBy(data, options = {}, writer = null) {
  const opts = {
    ...options,
    where: {
      ...options.where,
      deletedAt: { [Op.is]: null }
    },
    returning: true
  }
  const [count, rows] = await sequelize.models.mstrRoleModules.update(
    {
      updatedBy: writer || 'SYSTEM',
      updatedAt: await sql.getDatetime({
        ...{ transaction: options ? options.transaction : null },
      }),
      ...data
    },
    sql.constructOptions(opts, false)
  )

  return { count, rows }
}

module.exports = {
  create, getBy, updateBy, deleteBy
}
