// ! Just for example purpose!

const config = require('../../config');
const grpcClient = require('../../util/grpc-client');
const logger = require('../logging/createLoggerGrpc')(__filename);
const { stringify, objError } = require('../logging/format/common');

const { grpc: { serviceOpts } } = config;

const { TYPE_INFO, TYPE_ERROR } = config.constants;

const serviceModule = {
  name: 'Book', // Package name inside .proto
  service: 'BookMethod', // Service name inside .proto
  proto: `${__dirname}/../grpc/v1/proto/book.proto`,
  host: config.hosts.rpc.master
}

logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
  title: 'Load gRPC service',
  data: serviceModule
}))

const BookMethod = grpcClient(serviceModule, serviceOpts);

BookMethod.GetBooks(null, (err, response) => {
  if (err) {
    logger.error(stringify(TYPE_ERROR.GRPC_CLIENT, objError(err)));
  } else {
    logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
      title: 'BookMethod.GetBooks',
      data: response
    }))
  }
});

BookMethod.GetBookById({ id: 1 }, (err, response) => {
  if (err) {
    logger.error(stringify(TYPE_ERROR.GRPC_CLIENT, objError(err)));
  } else {
    logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
      title: 'BookMethod.GetBookById',
      data: response
    }))
  }
});

BookMethod.CreateBook({ id: -1, title: 'Cracking the Interview', chapters: 12 }, (err, response) => {
  if (err) {
    logger.error(stringify(TYPE_ERROR.GRPC_CLIENT, objError(err)));
  } else {
    logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
      title: 'BookMethod.CreateBook',
      data: response
    }))
  }
});

BookMethod.UpdateBookById({ id: 1, update: { title: 'yo', chapters: 16 } }, (err, response) => {
  if (err) {
    logger.error(stringify(TYPE_ERROR.GRPC_CLIENT, objError(err)));
  } else {
    logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
      title: 'BookMethod.UpdateBookById',
      data: response
    }))
  }
});

BookMethod.DeleteBookById({ id: 2 }, (err, response) => {
  if (err) {
    logger.error(stringify(TYPE_ERROR.GRPC_CLIENT, objError(err)));
  } else {
    logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
      title: 'BookMethod.DeleteBookById',
      data: response
    }))
  }
});

setTimeout(() => {
  BookMethod.GetBooks(null, (err, response) => {
    if (err) {
      logger.error(stringify(TYPE_ERROR.GRPC_CLIENT, objError(err)));
    } else {
      logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
        title: 'BookMethod.GetBooks',
        data: response
      }))
    }
  });
}, 5000)
