const Promise = require('bluebird');
const config = require('../../config');
const logger = require('../logging/createLoggerGrpc')(__filename);
const { stringify, objError } = require('../logging/format/common');
const grpcClient = require('../../util/grpc-client');

const { grpc: { serviceOpts } } = config;
const { TYPE_INFO, TYPE_ERROR } = config.constants;

module.exports = class RoleResourceGRPCClient {
  constructor() {
    this.module = {
      name: 'RoleResource', // Package name inside .proto
      service: 'RoleResourceMethod', // Service name inside .proto
      proto: `${__dirname}/../grpc/v1/proto/role-resource.proto`,
      host: config.hosts.rpc.master
    }

    logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
      title: 'Load gRPC service',
      data: this.module
    }))

    this.method = Promise.promisifyAll(grpcClient(this.module, serviceOpts));
  }

  async getAll() {
    const response = await this.method.GetRoleResourcesAsync(null)

    logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
      title: 'RoleResourceMethod.GetRoleResources',
      data: response
    }))

    return response
  }
}
