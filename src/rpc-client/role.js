const Promise = require('bluebird');
const config = require('../../config');
const logger = require('../logging/createLoggerGrpc')(__filename);
const { stringify, objError } = require('../logging/format/common');
const grpcClient = require('../../util/grpc-client');

const { grpc: { serviceOpts } } = config;
const { TYPE_INFO, TYPE_ERROR } = config.constants;

module.exports = class RoleGRPCClient {
  constructor() {
    this.module = {
      name: 'Role', // Package name inside .proto
      service: 'RoleMethod', // Service name inside .proto
      proto: `${__dirname}/../grpc/v1/proto/role.proto`,
      host: config.hosts.rpc.master
    }

    logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
      title: 'Load gRPC service',
      data: this.module
    }))

    this.method = Promise.promisifyAll(grpcClient(this.module, serviceOpts));
  }

  async getAll() {
    const response = await this.method.GetRolesAsync(null)

    logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
      title: 'RoleMethod.GetRoles',
      data: response
    }))

    return response
  }

  async getByCode(code) {
    const response = await this.method.GetRoleByCodeAsync({ code })

    logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
      title: 'RoleMethod.GetRoleByCode',
      data: response
    }))

    return response
  }
}
