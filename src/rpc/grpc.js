const Mali = require('mali');

const { constants: { TYPE_INFO, TYPE_ERROR, MESSAGE }, grpc } = require('../config');
const logger = require('../logging/createLoggerGrpc')(__filename);
const { stringify, objError } = require('../logging/format/common');
const modules = require('./v1/modules');
const Singleton = require('../../util/singleton');

class GRPC extends Singleton {
  constructor() {
    super();
    const me = this;

    me.logs = [];
    me.app = new Mali();

    Object.values(modules).forEach(mod => {
      me.app.addService(mod.proto, mod.service, grpc.serviceOpts);

      mod.handlers.forEach((handler) => {
        const middlewares = handler.middlewares || [];
        const actions = [
          mod.service,
          handler.method.name,
          ...middlewares,
          handler.method
        ]
        me.app.use(...actions)
      })
    })

    me.app.on('error', (error) => {
      if (!error.code) {
        logger.error(stringify(TYPE_ERROR.GRPC, objError(error)));
      }
    });
  }

  #printState(server) {
    if (server) {
      if (this.logs.length > 100) this.logs.shift();
      this.logs.push({ state: server.started ? 'start' : 'stop', date: new Date() });

      logger.info(stringify(
        TYPE_INFO.SYSTEM,
        `${server.started ? MESSAGE.GRPC_LISTENED : MESSAGE.GRPC_STOPPED} ${grpc.port}...`
      ));
    }
  }

  #keepAlive() {
    const me = this;
    if (!!me.polling && !(me.polling || {})._destroyed) clearInterval(me.polling);
    me.polling = setInterval(() => {
      // * Mali will always push when grpc server start, we just need to check last index of servers
      const { server } = me.app.servers[me.app.servers.length - 1]
      if (!server.started && !me.closed) {
        me.start().then(() => {
          logger.info(stringify(
            TYPE_INFO.SYSTEM,
            MESSAGE.GRPC_FORCE_STARTED
          ));
        })
      } else if (me.closed) {
        logger.info(stringify(
          TYPE_INFO.SYSTEM,
          MESSAGE.GRPC_FORCE_STOPPED
        ));
        clearInterval(me.polling);
      }
    }, 200)
  }

  async start() {
    const me = this;
    const { port } = grpc

    if (grpc.forever) me.#keepAlive();

    const server = await this.app.start(`0.0.0.0:${port}`);
    me.#printState(server);

    return server
  }

  async stop(force = false) {
    const me = this;
    this.closed = force;

    await this.app.close()

    // * Mali will always push when grpc server start, we just need to check last index of servers
    me.#printState(me.app.servers[me.app.servers.length - 1].server)

    return me.app.servers;
  }
}

module.exports = GRPC
