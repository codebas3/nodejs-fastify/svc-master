module.exports = {
  Book: require('./book'),
  Ping: require('./ping'),
  Enumeration: require('./enumeration'),
  Role: require('./role'),
  RoleResource: require('./role-resource'),
}
