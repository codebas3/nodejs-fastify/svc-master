/* eslint-disable object-curly-newline */

const name = 'RoleResource';
const service = 'RoleResourceMethod';
const proto = require('path').resolve(`${__dirname}/../proto/role-resource.proto`);

const RoleResourceService = require('../../../services/v1/role-resource');

const GetRoleResources = async (ctx, next) => {
  try {
    const roleResourceService = new RoleResourceService()
    const data = await roleResourceService.getBy({
      count: false,
      limit: -1
    });

    const idx = {};
    const rows = [];
    data.forEach((d) => {
      const { mstrResource, mstrRole } = d
      const { id, code, name: _name, isInternalDefault, isExternalDefault, notes } = mstrResource
      const { id: _id, code: _code, name: __name, isInternal, isDefault, notes: _notes } = mstrRole
      if (!idx[id]) {
        rows.push({
          key: code,
          value: { id, code, name: _name, isInternalDefault, isExternalDefault, notes, roles: [] }
        })
        idx[id] = { i: rows.length - 1, r: {} }
      }

      if (!idx[id].r[_id]) {
        rows[idx[id].i].value.roles.push({
          key: _code,
          value: { id: _id, code: _code, name: __name, isInternal, isDefault, notes: _notes }
        })
        idx[id].r[_id] = { i: rows[idx[id].i].value.roles.length - 1 }
      }
    })
    ctx.res = { rows };
  } catch (err) {
    throw new Error(err);
  }
};

module.exports = {
  name,
  proto,
  service,
  handlers: [
    { method: GetRoleResources }
  ]
};
