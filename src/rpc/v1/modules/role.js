/* eslint-disable object-curly-newline */

const name = 'Role';
const service = 'RoleMethod';
const proto = require('path').resolve(`${__dirname}/../proto/role.proto`);

const RoleService = require('../../../services/v1/role');

const GetRoles = async (ctx, next) => {
  try {
    const roleService = new RoleService()
    const data = await roleService.getBy({
      count: false,
      limit: -1
    });

    const rows = data.map(({ id, code, name: _name, isInternal, isDefault, notes }) => ({ id, code, name: _name, isInternal, isDefault, notes }))
    ctx.res = { rows };
  } catch (err) {
    throw new Error(err);
  }
};

const GetRoleByCode = async (ctx, next) => {
  try {
    const { code } = ctx.req;
    const roleService = new RoleService()
    const data = (await roleService.getBy({
      count: false,
      where: {
        code
      }
    }))[0];
    ctx.res = data;
  } catch (err) {
    throw new Error(err);
  }
}

module.exports = {
  name,
  proto,
  service,
  handlers: [
    { method: GetRoles },
    { method: GetRoleByCode }
  ]
};
