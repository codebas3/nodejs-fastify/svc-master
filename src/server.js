const path = require('path');
const Fastify = require('fastify')
const qs = require('qs')
const middie = require('@fastify/middie')
const helmet = require('@fastify/helmet');
const cors = require('@fastify/cors');
const _static = require('@fastify/static');
const _swagger = require('@fastify/swagger');
const _swaggerUI = require('@fastify/swagger-ui');
const http = require('http')

const { env, app, constants, grpc, swaggerUI, openAPI } = require('./config');
const logger = require('./logger/console')(__filename);
const { stringify } = require('./logger/lib/format');
const middleware = require('./api/v1/middleware');
const Singleton = require('./util/singleton');

const { TYPE_INFO, MESSAGE } = constants;

class Server extends Singleton {
  opts;

  http;

  app;

  /**
   * App Constructor
   * @param {Object} opts - required parameters to run the app
   */
  constructor(opts) {
    super();

    const me = this;
    const serverFactory = (handler, opts) => {
      me.http = http.createServer((req, res) => {
        handler(req, res)
      })

      return me.http
    }
    
    const fastify = Fastify({
      logger: false,
      serverFactory,
      querystringParser: str => qs.parse(str)
    });

    me.opts = opts;
    me.app = fastify

    fastify.register(middie).then(() => {
      fastify.register(_static, {
        root: path.join(__dirname, '..', 'dist')
      })

      // OpenAPI
      if (opts.docs) {
        fastify.register(_swagger, {
          openapi: openAPI,
          hideUntagged: false,
          exposeRoute: true
        })
        fastify.register(_swaggerUI, swaggerUI)
      }

      // Cors
      if (opts.corsOptions) fastify.register(cors, opts.corsOptions);

      // Content Security Policy
      me.directives = helmet.contentSecurityPolicy.getDefaultDirectives();
      delete me.directives['form-action'];
      if (env !== 'production') {
        delete me.directives['script-src'];
        delete me.directives['style-src'];
      } else {
        fastify.register(helmet, {
          contentSecurityPolicy: {
            useDefaults: false,
            directives: {
              ...me.directives,
              'script-src': ["'self'", "'unsafe-inline'", 'https://unpkg.com', 'https://cdnjs.cloudflare.com'],
              'style-src': ["'self'", "'unsafe-inline'", 'https://unpkg.com'],
            },
          },
        });
      }

      // User language
      fastify.use(middleware.language);

      // Http logger
      fastify.use(middleware.tracker.binder)
      fastify.addHook('preValidation', middleware.tracker.logging.request)
      fastify.addHook('onResponse', middleware.tracker.logging.response)

      // Routes
      if (opts.routes && opts.routes.length !== 0) {
        Object.values(opts.routes).forEach(definition => {
          definition.routes.forEach(route => {
            let url = definition.basePath + route.path;
            url = url.slice(-1) === '/' ? url.slice(0, -1) : url;

            const schema = {
              // ...route.validators
              querystring: {
                name: { type: 'string' },
                excitement: { type: 'integer' }
              },
              response: {
                200: {
                  type: 'object',
                  properties: {
                    hello: { type: 'string' }
                  }
                }
              }
            };

            fastify.route({
              url,
              method: route.method.toUpperCase(),
              preValidation: route.action.slice(0, -1),
              handler: route.action.slice(-1)[0],
              schema
            })
          })
        })
      }

      // // * views
      // fastify.set('view engine', 'ejs');
      // fastify.set('views', path.join(__dirname, 'pages', 'views'))
      // fastify.get('/', (req, res) => res.render('index', app));
      // fastify.get('/chat', (req, res) => res.render('chat'));

      // // * for default error fallback
      // fastify.use(middleware.error);
    })
  }

  async start() {
    await this.app.ready()
    
    // this.app.swagger()
    this.app.listen({ port: app.port }, () => logger.info(stringify(
      TYPE_INFO.SYSTEM,
      `${MESSAGE.HTTP_LISTENED} ${app.port}...`
    )))
  }

  async stop() {
    return this.http.close(() => logger.info(stringify(
      TYPE_INFO.SYSTEM,
      `${MESSAGE.HTTP_STOPPED} ${app.port}`
    )))
  }
}

module.exports = Server;
