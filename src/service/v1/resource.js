/* eslint-disable class-methods-use-this */

const logger = require('../../logger/console')(__filename);
const { stringify, objError } = require('../../logger/lib/format');
const { constants } = require('../../config');
const Sql = require('../../model/sql');

const { TYPE_INFO, TYPE_ERROR } = constants;

class Resource {
  caller() {
    // * This is fake error for getting where the method call from;
    // * in case, we can't use arguments.caller in strict mode
    const e = new Error();
    return e.stack.split('\n')[3].trim().split(/\s/g)[1]
  }

  logInfo(options, result) {
    logger.info(stringify(
      `${TYPE_INFO.SERVICE_V1}: ${this.caller()}`,
      ({
        returned: result.rows ? result.rows.length : result.length,
        options
      })
    ));
  }

  logError(err) {
    logger.error(stringify(
      `${TYPE_ERROR.SERVICE_V1}: ${this.caller()}`,
      objError(err)
    ));
  }

  async create(data, options, writer) {
    const { repo } = Sql.getInstance()
    const result = await repo.resource.create(data, options, writer);
    this.logInfo({ data, options, writer }, result)
    return result;
  }

  async getBy(options) {
    const { repo } = Sql.getInstance()
    const result = await repo.resource.getBy(options);
    this.logInfo({ options }, result)
    return result;
  }

  async deleteBy(options, writer) {
    const { repo } = Sql.getInstance()
    const result = await repo.resource.deleteBy(options, writer);
    this.logInfo({ options, writer }, result)
    return result;
  }

  async updateBy(data, options, writer) {
    const { repo } = Sql.getInstance()
    const result = await repo.resource.updateBy(data, options, writer);
    this.logInfo({ data, options, writer }, result)
    return result;
  }
}

module.exports = Resource
