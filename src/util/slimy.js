const _ = require('lodash')
const Singleton = require('./singleton')

class Slimy extends Singleton {
  constructor({
    MAX_STR_LEN = 1000, MAX_ARR_LEN = 3, MAX_OBJ_LEN = -1, SEPARATOR = '...'
  } = {}) {
    super();
    this.MAX_STR_LEN = MAX_STR_LEN;
    this.MAX_ARR_LEN = MAX_ARR_LEN;
    this.MAX_OBJ_LEN = MAX_OBJ_LEN;
    this.SEPARATOR = SEPARATOR;
  }

  static isIterable(data) {
    if (typeof data === 'object' && data instanceof Array) return true
    if (typeof data === 'object' && data instanceof Object) return true
    return false
  }

  #reduct(data) {
    let separator = _.clone(this.SEPARATOR);
    if (typeof data === 'string') {
      if (data.length <= this.MAX_STR_LEN) return data;

      const max = this.MAX_STR_LEN - separator.length;
      const ab = (max % 2) ? [(max + 1) / 2, (max - 1) / 2] : [max / 2, max / 2];

      separator = data.length <= this.MAX_STR_LEN ? data.substring(ab[0], ab[0] + separator.length) : separator;

      return `${data.substring(0, ab[0])}${separator}${data.substring(data.length - ab[1])}`
    }
    if (typeof data === 'object' && data instanceof Array) {
      if (data.map(el => typeof el === 'string').indexOf(false) === -1) return data;
      if (this.MAX_ARR_LEN === -1 || (data.length <= this.MAX_ARR_LEN)) return data;

      const max = this.MAX_ARR_LEN - 1;
      const ab = (max % 2) ? [(max + 1) / 2, (max - 1) / 2] : [max / 2, max / 2];

      separator = data.length <= this.MAX_ARR_LEN ? data.slice(ab[0], ab[0] + 1) : separator;

      return [].concat(
        data.slice(0, ab[0]),
        separator,
        data.slice(data.length - ab[1])
      )
    }
    if (typeof data === 'object' && data instanceof Object) {
      if (this.MAX_OBJ_LEN === -1 || (Object.keys(data).length <= this.MAX_OBJ_LEN)) return data;

      const max = this.MAX_OBJ_LEN - 1;
      const ab = (max % 2) ? [(max + 1) / 2, (max - 1) / 2] : [max / 2, max / 2];

      separator = data.length <= this.MAX_OBJ_LEN ? Object.entries(data).slice(ab[0], ab[0] + 1) : [[separator, separator]];

      return Object.fromEntries([].concat(
        Object.entries(data).slice(0, ab[0]),
        separator,
        Object.entries(data).slice(Object.entries(data).length - ab[1])
      ))
    }

    return data
  }

  #iterate(data) {
    const d = this.#reduct(data);
    if (!Slimy.isIterable(d)) return d;

    for (let i = 0; i < Object.keys(d).length; i++) {
      const key = Object.keys(d)[i];
      d[key] = this.#iterate(d[key])
    }

    return d;
  }

  reduction(data) {
    const d = _.cloneDeep(data);
    return this.#iterate(d);
  }
}

module.exports = Slimy;
