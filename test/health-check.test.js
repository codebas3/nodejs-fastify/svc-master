const request = require('supertest');

const SvcMaster = require('./mock/svc-master');

beforeAll(async () => {
  const scvMaster = await SvcMaster()
  Object.assign(this, { scvMaster })
});

afterAll(async () => {
  await this.scvMaster.disconnect()
  await this.scvMaster.grpc.stop(true);
});

describe('Test example', () => {
  test('GET /api/v1/healthcheck', (done) => {
    request(this.scvMaster.server.app)
      .get('/api/v1/healthcheck')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect((res) => {
        // res.body.data.forEach(d => console.log(d))
      })
      .end((err, res) => {
        if (err) return done(err);
        return done();
      });
  });
});
