require('./variable').mockAppPort('master', '2000')

const Manifest = require('../../src/manifest');

const manifest = Manifest.getInstance();

module.exports = async () => {
  await manifest.setup();
  await manifest.grpc.start();
  await manifest.setData();

  return manifest;
};
