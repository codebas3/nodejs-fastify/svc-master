module.exports = {
  owner: {
    sub: 'andaikan@nodomain.com',
    profile: {
      accountId: '1',
      usrRegistrantId: '1',
      uname: 'andaikan',
      email: 'andaikan@nodomain.com',
      phone: '+6286313149xxx',
      name: 'Andaikan',
    },
    roles: ['owner'],
    scope: 'offline_access',
    client_id: 'all-in-one-client',
    aud: 'http://localhost:3001/xyz',
  }
}
