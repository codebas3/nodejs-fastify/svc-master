require('dotenv').config();

const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const config = require('./src/config');

const modulePaths = module.paths.slice(0, 2);
const webpackOpts = {
  mode: config.env,
  context: path.resolve(__dirname, 'src', 'page'),
  entry: {
    'script/docs': './controller/docs/index.js',
    'script/chat': './controller/chat/index.js',
  },
  plugins: [
    new Dotenv({ silent: true }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: 'lib/*',
          noErrorOnMissing: true,
          filter: async (file) => {
            const exludes = ['.gitkeep'];
            if (exludes.indexOf(file.replace(/^.*[\\/]/, '')) === -1) return true

            return false;
          },
          to: path.resolve(__dirname, 'dist')
        },
        {
          from: 'css/*',
          noErrorOnMissing: true,
          to: path.resolve(__dirname, 'dist')
        },
        {
          from: 'asset/**/*',
          noErrorOnMissing: true,
          filter: async (file) => {
            const exludes = ['site.webmanifest'];
            if (exludes.indexOf(file.replace(/^.*[\\/]/, '')) === -1) return true

            return false;
          },
          to: path.resolve(__dirname, 'dist')
        },
        ...modulePaths.map(dir => ({
          from: `${dir}/swagger-ui-themes/themes`,
          noErrorOnMissing: true,
          to: path.resolve(__dirname, 'dist/lib/swagger')
        })),
      ],
    }),
  ],
}

if (config.env === 'development') {
  webpackOpts.devtool = 'cheap-module-source-map'
}

module.exports = webpackOpts;
